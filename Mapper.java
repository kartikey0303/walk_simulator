public class Mapper
{
    int camX;
    int camY;
    private int lineOfSight = 5;
    Map map;
    Tile[][] area = new Tile[2*lineOfSight+1][2*lineOfSight+1];
    public void render()
    {
        System.out.println("----------------------");
        for(int i=camX-lineOfSight; i<=camX+lineOfSight; i++)
        {
            for(int j=camY-lineOfSight; j<=camY+lineOfSight; j++)
            {
                int maxSize=map.getMaxMapSize();
                if(i==camX && j==camY)
                    System.out.print("C ");
                else if(i<0 || j<0 || i>=maxSize|| j>=maxSize)
                    System.out.print(map.def.img.img+" ");
                else
                    System.out.print(map.area[i][j].type.img.img+" ");                   
            }
            System.out.println();
        }
    }


    public void loadMap(Map m,int X, int Y)
    {
        map=m;
        camX=X;
        camY=Y;
        render();
    }
    public void setPos(int cX, int cY)
    {
        camX=cX;
        camY=cY;
    }

}
