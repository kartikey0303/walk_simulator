public class Map
{
    private static int max_map_size = 19;
    private static int max_textures = 10;
    public static TileType def = new TileType(false, new Image('O')); 
    public int getMaxTextures()
    {
        return max_textures;
    }

    public int getMaxMapSize()
    {
        return max_map_size;
    }
    TileType[] tileset = new TileType[max_textures];
    Tile[][] area = new Tile[max_map_size][max_map_size];

    public Map( Tile[][] t, TileType[] t1)
    {
        this.area=t;
        this.tileset=t1;
    }

}
