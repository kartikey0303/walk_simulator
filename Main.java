import java.awt.*;
import java.util.*;
public class Main
{
    public static void main(String args[])
    {
        Image grassImg=new Image(',');//later to be replaced by images
        Image dirtImg=new Image('.');
        Image wallImg=new Image('+');
        Image waterImg=new Image('~');
        TileType grass = new TileType(true, grassImg);
        TileType dirt = new TileType(true, dirtImg);
        TileType wall = new TileType(false, wallImg);
        TileType water=new TileType(false,waterImg);
        Scanner ob=new Scanner(System.in);
        TileType[] tileset = {water, grass, dirt, wall};//helps us to create more tiletypes easier
        Mapper mapper =new Mapper();
        Tile[][] area = {{new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[3])},
                {new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[2]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1])},
                {new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[2]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[2]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1])},
                {new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0])},
                {new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[2]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3])},
                {new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[2]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[2]), new Tile(tileset[2]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3])},
                {new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[2]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[2])},
                {new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[2]), new Tile(tileset[2]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[2])},
                {new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[2]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[2])},
                {new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[2]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[2])},
                {new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[2])},
                {new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[2]), new Tile(tileset[2]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[2]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[2])},
                {new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[2]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[2])},
                {new Tile(tileset[0]), new Tile(tileset[2]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[2]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[2])},
                {new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[2]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3])},
                {new Tile(tileset[0]), new Tile(tileset[2]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[2]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3])},
                {new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[2]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[2]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[2])},
                {new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[3])},
                {new Tile(tileset[1]), new Tile(tileset[3]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[0]), new Tile(tileset[2]), new Tile(tileset[3]), new Tile(tileset[0]), new Tile(tileset[1]), new Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[3]), new Tile(tileset[2]), new Tile(tileset[0]), new Tile(tileset[1]), new  Tile(tileset[1]), new Tile(tileset[0]), new Tile(tileset[3])}};
        Map map = new Map(area, tileset);
        int camX=5;
        int camY=5;
        mapper.loadMap(map,camX,camY);

        char c = ob.next().charAt(0);
        while(c!='e')
        {
            if(c=='d')
            {
                if(map.area[camX][camY+1].type.walkable)
                {
                    camY++;
                    System.out.print("\f");
                    mapper.setPos(camX,camY);
                    mapper.render();
                }
            }
            else if(c=='s')
            {
                if(map.area[camX+1][camY].type.walkable)
                {
                    camX++;
                    System.out.print("\f");
                    mapper.setPos(camX,camY);
                    mapper.render();
                }
            }
            else if(c=='a')
            {
                if(map.area[camX][camY-1].type.walkable)
                {
                    camY--;
                    System.out.print("\f");
                    mapper.setPos(camX,camY);
                    mapper.render();
                }
            }
            else if(c=='w')
            {
                if(map.area[camX-1][camY].type.walkable)
                {
                    camX--;
                    System.out.print("\f");
                    mapper.setPos(camX,camY);
                    mapper.render();
                }
            }
            else if(c!='a' || c!='w' || c!='s' || c!='d' || c!='e')
                System.out.print("\r\r");
            c = ob.next().charAt(0);
        }
    }
}